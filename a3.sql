
-- S.
-- This query returns data of all posts of profiles that I am subscribed to.

-- Get all posts data
SELECT PostID, Username, CreationTime, MediaTypeID, MediaFileUrl, NumberOfLikes
FROM Posts
WHERE PostID IN (
    -- Get a list of posts of my subscriptions
    SELECT ListOfPostIDs
    FROM Profiles
    WHERE ProfileID in (
        -- Get a list of profiles that I am subscribed to.
        SELECT SubscribedProfilesIDs
        FROM Profiles
        WHERE ProfileID = 'hardcorded_id_of_my_profile'
    );
);

-- S.
-- This script returns profile page
SELECT UserID, Username, Website, Bio, ProfileImageUrl, NumberOfPosts, NumberOfFollowers, NumberOfFollowedUsers
FROM Profiles
WHERE ProfileID = 'hardcoded_id_of_some_profile';

-- S.
-- Return profile page's posts' data
SELECT PostID, LocationName, MediaType, MediaFileUrl
FROM Posts
WHERE PostId IN (
    -- Get a list of profile page's posts' ids
    SELECT ListOfPostIDs
    FROM Profiles
    WHERE ProfileID = 'hardcoded_id_of_some_profile'
    LIMIT 1;
);
ORDER BY CreationTime DESC;

-- S.
-- Get post's data
SELECT PostID, Username, ProfileImageUrl, LocationName, Location, NumberOfLikes
FROM Posts
WHERE PostId = 'hardcoded_id';

-- S.
-- Get data of all medias of some post.
SELECT PostMediaID, MediaTypeID, MediaFileUrl
FROM PostMedias
WHERE PostMediaID IN (
    SELECT ListOfMediaIDs
    FROM Posts
    WHERE PostId = 'hardcoded_id';
);

-- S.
-- Get data of all comments of some post.
SELECT CommentID, Comment, CreationTime
FROM Comments
WHERE CommentID IN (
    SELECT ListOfCommentsIds
    FROM Posts
    WHERE PostId = 'hardcoded_id';
);

-- EOF
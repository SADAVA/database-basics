
-- S.
-- Public user profile page.
-- It shows user name, user total chirps and user total likes.
-- Let 17 be our hard coded id

SELECT
    (SELECT UserName FROM User WHERE UserID = '17') AS UserName,
    (SELECT COUNT(Chirp.ChirpID) FROM Chirp WHERE ChirpAuthor = '17') AS UserTotalChirps,
    (SELECT COUNT(`Like`.LikeID) FROM `Like` WHERE `Like`.LikeChirp IN (
        SELECT ChirpID FROM Chirp WHERE ChirpAuthor = '17')) AS TotalUserLikes;

-- S.
-- 17 is hardcoded id

-- Get chirps by authors I follow
SELECT ChirpText FROM Chirp WHERE ChirpAuthor IN (
    -- Get all authors whom I follow
    SELECT Following FROM Follow WHERE Follower = '17'
);
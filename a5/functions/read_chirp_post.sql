
-- S.
-- Read chirp data
-- Let 17 be our hard coded id

SELECT
    (SELECT ChirpText FROM Chirp WHERE ChirpID = '17') AS ChirpText,
    (SELECT COUNT(CommentID) FROM Comment WHERE CommentChirp = '17') AS ChirpCommentsCount,
    (SELECT Count(LikeID) FROM `Like` WHERE LikeChirp = '17') AS ChirpLikesCount;

-- Get chirp comments
SELECT CommentText FROM Comment WHERE CommentChirp = '17';

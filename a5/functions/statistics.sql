
-- S.
-- Read general statistics

SELECT
    (SELECT Count(UserID) FROM User) AS UserCount,
    (SELECT Count(ChirpID) FROM Chirp) AS ChirpCount,
    (SELECT Count(CommentID) FROM Comment) AS CommentCount,
    (SELECT Count(LikeID) FROM `Like`) AS LikeCount,
    (SELECT Count(FollowID) FROM Follow) AS FollowCount,
    (SELECT Avg(ChirpCount) FROM (SELECT ChirpAuthor, Count(ChirpID) AS ChirpCount FROM Chirp GROUP BY ChirpAuthor) ChirpsPerUser)
        AS AvgChirpsPerUser,
    (SELECT Max(ChirpCount) FROM (SELECT ChirpAuthor, Count(ChirpID) AS ChirpCount FROM Chirp GROUP BY ChirpAuthor) ChirpsPerUser)
        AS MaxChirpsPerUser,
    (SELECT Avg(FollowCount) FROM (SELECT Count(FollowID) AS FollowCount FROM Follow GROUP BY Follow.Following) ChirpsPerUser)
        AS AvgFollowsPerUser,
    (SELECT Avg(CommentCount) FROM (SELECT Count(CommentID) AS CommentCount FROM Comment GROUP BY CommentChirp) CommentsPerChirp)
        AS AvgCommentsPerChirp,
    (SELECT Max(CommentCount) FROM (SELECT Count(CommentID) AS CommentCount FROM Comment GROUP BY CommentChirp) CommentsPerChirp)
        AS MaxCommentsPerChirp,
    (SELECT Avg(LikeCount) FROM (SELECT Count(LikeChirp) AS LikeCount FROM `Like` GROUP BY LikeChirp) LikesPerChirp)
        AS AvgLikesPerChirp,
    (SELECT Max(LikeCount) FROM (SELECT Count(LikeChirp) AS LikeCount FROM `Like` GROUP BY LikeChirp) LikesPerChirp)
        AS MaxLikesPerChirp;

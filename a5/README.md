
# S.

## **The Chirpper**

This is a "reverse engineered" version of Twitter. Let's call it... **Chirpper**.  
Public git repository url: https://bitbucket.org/SADAVA/database-basics/src/master/a5/  
  
### Legend
**Chirpper** is a place where people can quiqkly share their thoughts and ideas, or simply their mood or events, in a simple text form. No mess, no photos, no inconsistency, just text.


**Important**: out of the box insert_data.sql contains just a little of data, so do not forget to generate data for this database, use script queries/generate.js to do that.  

Tables that we have:

 - **User**, who posts "chirps"
 - **Chirp**, actual posts
 - **Comment**, user comments to chirps
 - **Like**, user likes to chirps
 - **Follow**, user follows user relation
 - **Rechirp**, Link to some chirp

Functionality:

 - Users create **chirps**.
 - Users write **comments** on chirps.
 - Users put **likes** on chirps.
 - Users **follow** each other.
 - Users **rechirp** someone else's chirp to their own feed

To do:

 - Allow comments and likes to comments.

### EOF
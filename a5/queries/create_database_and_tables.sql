
-- S.
-- Usage: mysql -u username < create_database_and_tables.sql

DROP DATABASE IF EXISTS Chirpper;
CREATE DATABASE Chirpper;

CREATE TABLE Chirpper.User (
    UserID INT NOT NULL AUTO_INCREMENT,
    UserName CHAR(40) NOT NULL,
    UserCreateTS TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UserLastOnlineTS TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY (UserID)
);

CREATE TABLE Chirpper.Chirp (
    ChirpID INT NOT NULL AUTO_INCREMENT,
    ChirpText CHAR(255) NOT NULL,
    ChirpAuthor INTEGER NOT NULL,
    ChirpPostTS TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    ChirpLastEditTS TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

    PRIMARY KEY (ChirpID),
    FOREIGN KEY (ChirpAuthor) REFERENCES Chirpper.User(UserID)
);

CREATE TABLE Chirpper.Comment (
    CommentID INT NOT NULL AUTO_INCREMENT,
    CommentAuthor INT NOT NULL,
    CommentText CHAR(255) NOT NULL,
    CommentChirp INTEGER NOT NULL,
    CommentPostTS TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CommentLastEditTS TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

    PRIMARY KEY (CommentID),
    FOREIGN KEY (CommentAuthor) REFERENCES Chirpper.User(UserID),
    FOREIGN KEY (CommentChirp) REFERENCES Chirpper.Chirp(ChirpID)
);

CREATE TABLE Chirpper.Like (
    LikeID INT NOT NULL AUTO_INCREMENT,
    LikeAuthor INTEGER NOT NULL,
    LikeChirp INTEGER NOT NULL,
    LikeTS TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY (LikeID),
    FOREIGN KEY (LikeAuthor) REFERENCES Chirpper.User(UserID),
    FOREIGN KEY (LikeChirp) REFERENCES Chirpper.Chirp(ChirpID)
);

CREATE TABLE Chirpper.Follow (
    FollowID INT NOT NULL AUTO_INCREMENT,
    Follower INTEGER NOT NULL,
    Following INTEGER NOT NULL,
    FollowTS TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY (FollowID),
    FOREIGN KEY (Follower) REFERENCES Chirpper.User(UserID),
    FOREIGN KEY (Following) REFERENCES Chirpper.User(UserID)
);

CREATE TABLE Chirpper.Rechirp (
    RechirpID INT NOT NULL AUTO_INCREMENT,
    RechirpAuthor INTEGER NOT NULL,
    RechirpChirp INTEGER NOT NULL,
    RechirpTS TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY (RechirpID),
    FOREIGN KEY (RechirpAuthor) REFERENCES Chirpper.User(UserID),
    FOREIGN KEY (RechirpChirp) REFERENCES Chirpper.Chirp(ChirpID)
);
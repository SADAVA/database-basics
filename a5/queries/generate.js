
/*****************************
 *                           *
 *            S.             *
 *                           *
 *****************************/

// Use this script to generate new data for Chirpper database
// Usage:
// node generate.js > insert_data.sql

// WARNING:
// Once in a while for no apparent reason, mysql throws a syntax error at line:
// ---- INSERT INTO Chirpper.Rechirp (RechirpAuthor, RechirpChirp, RechirpTS)
// And I have no idea why, I checked everything and still could not figure out what's wrong.

// To import sql, do that with `create_database_and_tables.sql` if you have not that already.
// mysql -u username -p < insert_data.sql

// TimeStamping: each time timestamp is generated it is increment by some random range of seconds.


/*****************************
 *                           *
 *          SETTINGS         *
 *                           *
 *****************************/

const NO_OF_USERS = 500
const NO_OF_CHIRPS = 2000
const NO_OF_COMMENTS = 3000
const NO_OF_LIKES = 10000
const NO_OF_RECHIRPS = 300
const FOLLOWERS_MAX = 30 // With default settings it produces: AvgFollowsPerUser: 46
const FOLLOWERS_NONE_CHANCE = 0.2

// Number of words    [min, max]
const USERNAME_SIZE = [1, 4]
const POST_SIZE = [3, 30]
const COMMENT_SIZE = [1, 10]
const TIMESTAMP_INCREMENT = [1, 5 * 60] // in seconds

// Maximum chars
const USERNAME_MAX_LENGTH = 40
const POSTTEXT_MAX_LENGTH = 255
const COMMENT_MAX_LENGTH = 255

// This is our dictionary for everything: for user's names and post's contents
// 256 words of lorem ipsum without , or . symbols.
const DICTIONARY = "Lorem ipsum dolor sit amet consectetur adipiscing elit Ut tempor risus eu molestie dictum Nam tincidunt urna at leo posuere efficitur Duis massa risus fringilla id lacus eu iaculis cursus lorem Sed pharetra lacus sit amet malesuada ultrices Mauris feugiat rhoncus lacus vitae luctus Donec sed velit quis metus euismod congue Proin mollis lorem sit amet est vulputate ut venenatis metus imperdiet Duis posuere ipsum at faucibus sagittis lorem massa euismod neque id blandit purus purus sed orci Vivamus at ligula dapibus pellentesque turpis ac malesuada risus\
Phasellus euismod tristique tellus interdum consequat Mauris at justo bibendum condimentum felis in scelerisque lacus Suspendisse tristique neque magna auctor porta ante mollis ac Sed vestibulum leo erat eu tincidunt nisi fringilla sit amet Mauris erat justo pulvinar vel cursus malesuada tempor eget massa Proin sed neque ut tellus sagittis laoreet Praesent eget luctus ipsum Fusce et sem sapien Nunc dui leo consectetur ultrices posuere vitae pharetra at sem\
Curabitur dapibus congue lorem eget viverra Sed maximus pellentesque augue mollis ultrices Aenean vitae ipsum eget ipsum bibendum posuere Mauris rutrum sem a neque maximus cursus vehicula nisi volutpat Aenean porta nisl sit amet nulla convallis id bibendum magna porta Vestibulum bibendum auctor diam at dapibus Donec eget est non velit sollicitudin hendrerit Curabitur at facilisis tellus Praesent ex enim mattis et rutrum sed dignissim sed dolor Cras condimentum arcu sed eros vehicula venenatis Aenean nulla arcu fermentum id neque vel tincidunt condimentum nisl Integer cursus ante magna eget tempus dolor laoreet vitae Sed bibendum ex et luctus porta Mauris"
const list_of_words = DICTIONARY.split(" ")

// From where to start all timestamps
let currentDate = new Date()



/*****************************
 *                           *
 *      DATA GENERATION      *
 *                           *
 *****************************/

// Generate users
console.log("INSERT INTO Chirpper.User (UserName, UserCreateTS)")
console.log("VALUES")

for (let i = 0; i < NO_OF_USERS; i++) {
    // Generate random set of words joined with "_"
    const UserNameSize = randomRangeInt(...USERNAME_SIZE)
    const UserName = generateRandomCombination(UserNameSize, "_")
    const UserCreateTS = generateTimeStampForSQL()

    // Keep limits
    if (UserName.length > USERNAME_MAX_LENGTH)
        UserName = UserName.slice(0, USERNAME_MAX_LENGTH)

    // If this is the last line then put  ";" at the end, otherwise ","
    const end = (i === NO_OF_USERS - 1) ? ";" : ","

    console.log(`\t('${UserName}', '${UserCreateTS}')` + end)
}

console.log()

// Generate posts (chirps)
console.log("INSERT INTO Chirpper.Chirp (ChirpText, ChirpAuthor, ChirpPostTS)")
console.log("VALUES")

for (let i = 0; i < NO_OF_CHIRPS; i++) {
    const ChirpTextSize = randomRangeInt(...POST_SIZE)
    const ChirpText = generateRandomCombination(ChirpTextSize)
    const ChirpAuthor = randomRangeInt(0, NO_OF_USERS) + 1
    const ChirpPostTS = generateTimeStampForSQL()

    // Keep limits
    if (ChirpText.length > POSTTEXT_MAX_LENGTH)
        ChirpText = ChirpText.slice(0, POSTTEXT_MAX_LENGTH)

    // If this is the last line then put  ";" at the end, otherwise ","
    const end = (i === NO_OF_CHIRPS - 1) ? ";" : ","

    console.log(`\t('${ChirpText}', ${ChirpAuthor}, '${ChirpPostTS}')` + end)
}

console.log()

// Generate comments
console.log("INSERT INTO Chirpper.Comment (CommentAuthor, CommentText, CommentChirp, CommentPostTS)")
console.log("VALUES")

for (let i = 0; i < NO_OF_COMMENTS; i++) {
    const CommentAuthor = randomRangeInt(NO_OF_USERS) + 1
    const CommentTextSize = randomRangeInt(...COMMENT_SIZE)
    const CommentText = generateRandomCombination(CommentTextSize)
    const CommentChirp = randomRangeInt(NO_OF_CHIRPS) + 1
    const CommentPostTS = generateTimeStampForSQL()

    // Keep limits
    if (CommentText.length > COMMENT_MAX_LENGTH)
        CommentText = CommentText.slice(0, COMMENT_MAX_LENGTH)

    // If this is the last line then put  ";" at the end, otherwise ","
    const end = (i === NO_OF_COMMENTS - 1) ? ";" : ","

    console.log(`\t('${CommentAuthor}', '${CommentText}', '${CommentChirp}', '${CommentPostTS}')` + end)
}

console.log()

// Generate likes
console.log("INSERT INTO Chirpper.Like (LikeAuthor, LikeChirp, LikeTS)")
console.log("VALUES")

for (let i = 0; i < NO_OF_LIKES; i++) {
    const LikeAuthor = randomRangeInt(NO_OF_USERS) + 1
    const LikeChirp = randomRangeInt(NO_OF_CHIRPS) + 1
    const LikeTS = generateTimeStampForSQL()

    // If this is the last line then put  ";" at the end, otherwise ","
    const end = (i === NO_OF_LIKES - 1) ? ";" : ","

    console.log(`\t('${LikeAuthor}', '${LikeChirp}', '${LikeTS}')` + end)
}

console.log()

// Generate followers
console.log("INSERT INTO Chirpper.Follow (Follower, Following, FollowTS)")
console.log("VALUES")

for (let i = 0; i < NO_OF_CHIRPS; i++) {
    const followers_count = complexRandomInt(FOLLOWERS_MAX)

    for (let j = 0; j < followers_count; j++) {
        const Follower = randomRangeInt(NO_OF_USERS) + 1
        const Following = randomRangeInt(NO_OF_USERS) + 1
        const FollowTS = generateTimeStampForSQL()

        // If this is the last line then put  ";" at the end, otherwise ","
        const end = (i === NO_OF_CHIRPS - 1 && j === followers_count - 1) ? ";" : ","

        if (end !== ";") {
            // Make it so that some chirps do no have any comments
            if (Math.random() < FOLLOWERS_NONE_CHANCE) continue

            // Ignore self follow
            if (Follower === Following) continue
        }

        console.log(`\t('${Follower}', '${Following}', '${FollowTS}')` + end)
    }
}

console.log()

// Generate RECHIRPS
console.log("INSERT INTO Chirpper.Rechirp (RechirpAuthor, RechirpChirp, RechirpTS)")
console.log("VALUES")

for (let i = 0; i < NO_OF_RECHIRPS; i++) {
    const RechirpAuthor = randomRangeInt(NO_OF_USERS) + 1
    const RechirpChirp = randomRangeInt(NO_OF_CHIRPS) + 1
    const RechirpTS = generateTimeStampForSQL()

    // If this is the last line then put  ";" at the end, otherwise ","
    const end = (i === NO_OF_RECHIRPS - 1) ? ";" : ","

    console.log(`\t('${RechirpAuthor}', '${RechirpChirp}', '${RechirpTS}')` + end)
}


/*****************************
 *                           *
 *          UTILS            *
 *                           *
 *****************************/

function randomRangeInt(min, max) {
    if (max === undefined) {
        max = min
        min = 0
    }

    return Math.floor(Math.random() * (max - min)) + min
}

// Generates a string of random words of dictionary joined with "_"
function generateRandomCombination(size = 3, joiner = " ") {
    const random_index = () => randomRangeInt(list_of_words.length)
    const random_word = () => list_of_words[random_index()]

    const result = []

    for (let i = 0; i < size; i++)
        result.push(random_word())

    return result.join(joiner)
}

function complexRandomInt(max) {
    if (typeof max !== "number") throw new TypeError()
    // Don't ask me, I just wanted some more-or-less complex random
    return Math.floor(Math.sqrt(Math.pow(max, 2) * Math.pow(Math.random(), 2)))
}

function incrementDate(date, milliseconds) {
    date.setTime(date.getTime() + milliseconds)
}

function generateTimeStampForSQL() {
    incrementTime = randomRangeInt(...TIMESTAMP_INCREMENT) * 1000
    incrementDate(currentDate, incrementTime)

    return parseDateIntoSQLformat(currentDate)
}

function parseDateIntoSQLformat(date) {
    return date.toISOString()
        .replace(/T/, ' ')
        .replace(/\..+/, '')
}

/*****************************
 *                           *
 *            EOF            *
 *                           *
 *****************************/